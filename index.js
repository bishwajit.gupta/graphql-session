const { ApolloServer, gql } = require('apollo-server');

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Book" type defines the queryable fields for every book in our data source.
  type Book {
    title: String
    author: String
  }

  type Author{
    id: Int
    name: String
    books: [Book]
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query {
    books: [Book],
    authors: [Author]
  }

  type Mutation {
    addBook(title: String, author: String): Book!
  }

`;


let books = [
  {
    title: 'The Awakening',
    author: 'Kate Chopin',
  },
  {
    title: 'City of Glass',
    author: 'Paul Auster',
  },
];

let authors = [
  {
    id: 1,
    name: 'Kate Chopin',
    books: [
      {
        title: 'The Awakening'
      }
    ] 
  },
  {
    id: 2,
    name: 'Paul Auster',
    books: [
      {
        title: 'City of Glass',
      }
    ]
  },
];

  // Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const resolvers = {
    Query: {
      books: () => books,
      authors:() => authors
    },
    Mutation: {
      // 2
      addBook: (parent, args) => {
         const temp = {
          title: args.title,
          author: args.author,
        }
        books.push(temp)
        return temp
      }
    },
  };

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});


/**
 mutation {
  addBook(
    title: "Test Book",
    author: "Author 1"
  ){
    title
  }
}
 
*/